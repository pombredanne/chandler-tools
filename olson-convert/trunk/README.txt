Olson-convert

Copyright (c) 2006-2007Open Source Applications Foundation

open source, see LICENSE.txt file for details

Requires Python 2.4

The following Python Libraries are also required:

    pytz            http://pytz.sourceforge.net
    vobject-0.4.3+  http://vobject.skyhouseconsulting.com/

Both should be installed via easy_install.

vobject 0.4.3 or higher is required because I commited a patch to get iCal serialize to work with pytz timezone objects.

Note: The pytz homepage seems to be down at the moment, but 'easy_install pytz' seems to still work.

Full documentation is available on the OSAF Wiki:

    http://wiki.osafoundation.org/bin/view/Projects/OlsonConvert

Olson-convert is currently a single script that will convert on date/time/timezone into any other timezone using the olson timezone database. 

To run olson-convert and see what options are available:

    $ python olson-convert.py --help
