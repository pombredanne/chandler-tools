#!/usr/bin/env python

from setuptools import setup

desc = """
Olson-convert is currently a single script that will convert on 
date/time/timezone into any other timezone using the olson timezone database.
Supports input ics file and will output valid ics with VTIMEZONEs changed.
"""

setup(name='olson-convert',
      version='0.2',
      description=desc,
      summary=desc,
      author='Open Source Applications Foundation',
      author_email='mikeal@osafoundation.org',
      
      install_requires = ['pytz',
                          'vobject >= 0.4.3'],
      
      url='',
      license='http://www.apache.org/licenses/LICENSE-2.0',
      packages=[],
      platform=['Any'],
      classifiers=['Development Status :: 4 - Beta',
                   'Environment :: Library',
                   'Intended Audience :: Developers',
                   'License :: OSI Approved :: Apache Software License',
                   'Operating System :: OS Independent',
                   'Topic :: Software Development :: Libraries :: Python Modules',
                  ]
     )

