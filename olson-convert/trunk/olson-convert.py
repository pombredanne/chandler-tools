#!/usr/bin/python
#
#   Copyright (c) 2006-2007Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

"""
Date/Time/Timezone convertion tool -- Currently is a single script that will convert based on the olson timezone database. 
"""

__author__  = 'Mikeal Rogers <mikeal@osafoundation.org>'
__version__ = '0.1'

import os
import sys
from optparse import OptionParser
from datetime import datetime, timedelta
import pytz

DATETIME_OBJECT = datetime.now()

def init_options(**kwds):
    """
    Load and parse the command line options, with overrides in **kwds.
    Returns options
    
    This code is mostly stolen from Chandler @ OSAF.
    """

    _configItems = {        
        'full_datetime': ('-l', '--full-datetime', 's', None, None, 'The date and time and timezone you wish to convert. Example: US/Pacific:20050224T104500'),
        'datetime':      ('-u', '--datetime', 's', None, None, 'The date and time you wish to convert. Example: 20050224T154500'),
        'date':          ('-d', '--date', 's', None, None, 'The date you wish to convert. Example: 20050224'),
        'time':          ('-t', '--time', 's', None, None, 'The time you wish to convert. Example: 104500'),
        'from_tz':       ('-f', '--from-tz', 's', 'UTC', None, 'The source timezone, defualts to UTC'),
        'to_tz':         ('-z', '--to-tz', 's', None, None, 'The time to convert the date and time to'),
        'print_valid':   ('-v', '--print-valid', 'b', False, None, 'Print all valid timezones'),
        'dst':           ('-s', '--no-dst', 'b', True, None, 'Specify when local times should _not_ be in DST (Daylight Savings Time)'),
        'input_file':    ('-i', '--input', 's', None, None, 'Input file mode, converts all timezones in input iCalendar file to specified timezone'),
        'output_file':   ('-o', '--output', 's', None, None, 'Output file, used during input filemode. Name of file you wish to write converted iCalendar file, if not specified file is written to stdout'),
    }
    
    usage  = "usage: %prog [options]"  # %prog expands to os.path.basename(sys.argv[0])
    parser = OptionParser(usage=usage, version="%prog")

    for key in _configItems:
        (shortCmd, longCmd, optionType, defaultValue, environName, helpText)  = _configItems[key]

        if environName and os.environ.has_key(environName):
            defaultValue = os.environ[environName]

        if optionType == 'b':
            parser.add_option(shortCmd,
                              longCmd,
                              dest=key,
                              action='store_true',
                              default=defaultValue,
                              help=helpText)
        else:
            parser.add_option(shortCmd,
                              longCmd,
                              dest=key,
                              default=defaultValue,
                              help=helpText)

    (options, args) = parser.parse_args()
        
    for (opt,val) in kwds.iteritems():
        setattr(options, opt, val)
    
    options.args = args  # Store up the remaining args
    return options
    
def convert_v_list(vlist, tz):
        
    for vitem in vlist:
        try:
            vitem.dtstart.value = vitem.dtstart.value.astimezone(tz)
        except AttributeError:
            pass
        try:
            vitem.dtend.value = vitem.dtend.value.astimezone(tz)
        except AttributeError:
            pass
    
    return vlist
    
    
def convert_input_file(olsonconvert_options):
    
    import vobject
    
    try:
        f = open(olsonconvert_options.input_file, 'r')
    except:
        print 'Cannot open input file'
        sys.exit
    
    cal = vobject.readOne(f.read())
    
    new_cal = vobject.newFromBehavior('vcalendar')
    
    try:
        tz = pytz.timezone(olsonconvert_options.to_tz)
    except:
        print 'Timezone to convert to invalid, please use --print-valid to see a list of valid timezones'
        sys.exit()
        
    tz.tzid = tz.zone
    tz._tzid = tz.zone
        
    try:
        vevents = convert_v_list(cal.vevent_list, tz)
        for ev in vevents:
            new_cal.add(ev)
    except AttributeError:
        pass
    
    try:
        vtodos = convert_v_list(cal.vtodo_list, tz)
        for vt in vtodos:
            new_cal.add(vt)
    except AttributeError:
        pass
            
    try:
        valarms = convert_v_list(cal.valarm_list, tz)
        for va in vlarms:
            new_cal.add(va)
    except AttributeError:
        pass
    
    if olsonconvert_options.output_file:
        try:
            import codecs
            f = codecs.open(olsonconvert_options.output_file, 'w', 'utf-8')
        except:
            print 'Could not open file for writing'
            sys.exit()
        
        f.write(new_cal.serialize())
        print 'File write complete'
        sys.exit()
    else:
        print new_cal.serialize().encode('utf-8', 'ignore')
        sys.exit()

    
def main():
        
    olsonconvert_options = init_options()
    
    if olsonconvert_options.print_valid is True:
        for tz in pytz.all_timezones:
            print tz
        sys.exit() 
        
    if olsonconvert_options.input_file is not None:
        convert_input_file(olsonconvert_options)
    
    if olsonconvert_options.full_datetime is not None:
        split = olsonconvert_options.full_datetime.split(':')
        olsonconvert_options.from_tz = split[0]
        olsonconvert_options.datetime = split[1]
    
    if olsonconvert_options.datetime is not None:
        split = olsonconvert_options.datetime.split('T')
        olsonconvert_options.date = split[0]
        olsonconvert_options.time = split[1]
        
    if olsonconvert_options.date is None or olsonconvert_options.time is None:
        print 'Please give a time to convert'
        sys.exit()
        
    if olsonconvert_options.to_tz is None:
        print 'Please give a timezone to convert to'
        sys.exit()
    
    try:
        to_tz = pytz.timezone(olsonconvert_options.to_tz)
    except:
        print 'Timezone to convert to invalid, please use --print-valid to see a list of valid timezones'
        sys.exit()
    try:
        from_tz = pytz.timezone(olsonconvert_options.from_tz)
    except:
        print 'Timezone to convert from invalid, please use --print-valid to see a list of valid timezones'
        sys.exit()
    
    date = olsonconvert_options.date
    time = olsonconvert_options.time 
        
    year = int(date[0]+date[1]+date[2]+date[3])
    month = int(date[4]+date[5])
    day = int(date[6]+date[7])
    hour = int(time[0]+time[1])
    minute = int(time[2]+time[3])
    second = int(time[4]+time[5])
    
    dt = datetime(year, month, day, hour, minute, second)
    fmt = '%Y-%m-%d %H:%M:%S %Z%z'
    
    local = from_tz.localize(dt, is_dst=olsonconvert_options.dst)
    
    print '==The following time in %s' % olsonconvert_options.from_tz
    print local.strftime(fmt)
    print '==Converted to %s is==' % olsonconvert_options.to_tz
    
    convertion = local.astimezone(to_tz)
    print convertion.strftime(fmt)
    
if __name__ == '__main__':
    
    main()
    
    
    
    
    
    
    
    
     