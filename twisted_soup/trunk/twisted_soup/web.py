# Copyright (c) 2007 Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import copy
import cgi

def initialize():
    from twisted.web2 import wsgi
    from twisted.web2.server import Site
    from twisted.web2.channel import HTTPFactory
    from twisted_soup import extensions
    from twisted.internet import reactor
    import twisted_soup
    
    application = WSGISoupApplication()
    
    for extension in [getattr(extensions, ext) for ext in dir(extensions) if (
                      hasattr(getattr(extensions, ext), '_wsgi_web_app_') is True)]:
        for name, value in extension._wsgi_web_app_.items():
            application.mount_wsgi_app(name, value)
    
    resource = wsgi.WSGIResource(application)
    site = Site(resource)
    http_factory = HTTPFactory(site)
    reactor.listenTCP(twisted_soup.settings['web_port'], http_factory)

def reconstruct_url(environ):
    from urllib import quote
    url = environ['wsgi.url_scheme']+'://'
    if environ.get('HTTP_HOST'): url += environ['HTTP_HOST']
    else:
        url += environ['SERVER_NAME']
        if environ['wsgi.url_scheme'] == 'https':
            if environ['SERVER_PORT'] != '443':
               url += ':' + environ['SERVER_PORT']
        else:
            if environ['SERVER_PORT'] != '80':
               url += ':' + environ['SERVER_PORT']
    url += quote(environ.get('SCRIPT_NAME',''))
    url += quote(environ.get('PATH_INFO','')).replace(url.replace(':', '%3A'), '')
    if environ.get('QUERY_STRING'):
        url += '?' + environ['QUERY_STRING']
    environ['reconstructed_url'] = url
    return url 

class WSGISoupApplication(object):
    def __init__(self):
        self.resource_map = {}
        
    def mount_wsgi_app(self, rest, app):
        """Mount the given wsgi application to the given rest interface"""
        self.resource_map[rest] = app
        
    def handler(self, environ, start_response):
        reconstruct_url(environ)
        
        if environ['REQUEST_METHOD'].upper() == 'POST':
            get_post_form(environ)
        
        if environ['PATH_INFO'] in self.resource_map.keys():
            return self.resource_map[environ['PATH_INFO']](environ, start_response)
        else:
            rest_list = environ['PATH_INFO'].split('/')
            while '' in rest_list:
                rest_list.remove('')
            args = []
            for i in range(len(copy.copy(rest_list))):
                if self.resource_map.has_key('/'.join(rest_list)):
                    args.reverse()
                    return self.resource_map['/'.join(rest_list)](environ, start_response, *args)
                args.append(rest_list.pop(-1))
            start_response("404 Not Found", [('Content-Type', 'text/html')])
            return ['<H1>Could not find resource</H1>']
            
    def __call__(self, environ, start_response):
        return self.handler(environ, start_response)
        
# From Ian Bickings' specification http://wsgi.org/wsgi/Specifications/handling_post_forms
def is_post_request(environ):
    if environ['REQUEST_METHOD'].upper() != 'POST':
        return False
    content_type = environ.get('CONTENT_TYPE', 'application/x-www-form-urlencoded')
    return ( content_type.startswith('application/x-www-form-urlencoded') or content_type.startswith('multipart/form-data') )

class InputProcessed(object):
    def read(self, *args):
        raise EOFError('The wsgi.input stream has already been consumed')
    readline = readlines = __iter__ = read

def get_post_form(environ):
    assert is_post_request(environ)
    wsgi_input = environ['wsgi.input']
    post_form = environ.get('wsgi.post_form')
    if (post_form is not None
        and post_form[0] is wsgi_input):
        return post_form[2]
    # This must be done to avoid a bug in cgi.FieldStorage
    environ.setdefault('QUERY_STRING', '')
    fs = cgi.FieldStorage(fp=wsgi_input,
                          environ=environ,
                          keep_blank_values=1)
    new_input = InputProcessed()
    post_form = (new_input, wsgi_input, fs)
    environ['wsgi.post_form'] = post_form
    environ['wsgi.input'] = new_input
    return fs
