# Copyright (c) 2007 Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import os, sys                 
                     
module_names = [module_name.replace('.py', '') for module_name in 
                os.listdir(os.path.dirname(os.path.abspath(sys.modules[__name__].__file__))) if (
                not module_name.startswith('.') and not module_name.startswith('_') 
                and module_name.endswith('.py') ) or ( os.path.isdir( os.path.dirname( 
                os.path.abspath( sys.modules[__name__].__file__))+os.path.sep+module_name ) 
                and not module_name.startswith('.') and not module_name.startswith('_') ) ]

for module_name in module_names: 
    __import__(__name__+'.'+module_name)

delattr(sys.modules[__name__], 'module_names')
delattr(sys.modules[__name__], 'module_name')
delattr(sys.modules[__name__], 'os')
delattr(sys.modules[__name__], 'sys')
