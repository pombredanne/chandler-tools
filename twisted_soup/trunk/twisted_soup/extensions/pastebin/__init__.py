# Copyright (c) 2007 Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import sys, os, copy
import wsgi_fileserver
import sqlobject
from mako.template import Template
from mako.lookup import TemplateLookup
from pygments import highlight
from pygments.formatters import HtmlFormatter
import pygments.lexers
import twisted_soup

lexer_list = []
lexer_names = []
for x in pygments.lexers.get_all_lexers():
    lexer_list.append(x)
    lexer_names.append(x[0])

lexer_names.sort()
lexer_names.insert(0, lexer_names.pop(lexer_names.index('Python')))
lexer_names.insert(1, lexer_names.pop(lexer_names.index('Python Traceback')))
lexer_names.insert(2, lexer_names.pop(lexer_names.index('JavaScript')))
lexer_names.insert(3, lexer_names.pop(lexer_names.index('Java')))
lexer_names.remove('Brainfuck')
language_list = copy.copy(lexer_names)
language_list.insert(0, '')
channel_list = copy.copy(twisted_soup.settings['irc_channels'])
channel_list.insert(0, '')

files_dir = os.path.abspath(os.path.dirname(sys.modules[__name__].__file__))
mako_lookup = TemplateLookup(directories=[files_dir], output_encoding='utf-8', encoding_errors='ignore')
fileserver_application = wsgi_fileserver.WSGIFileServerApplication(root_path=files_dir, mount_point='/paste/')

def provision():    
    paste = Paste(author='test', title='test title', language='Python', text=open(sys.modules[__name__].__file__.replace('.pyc', '.py')).read())


class Paste(sqlobject.SQLObject):
    author = sqlobject.StringCol(default=None)
    title = sqlobject.StringCol(default=None)
    language = sqlobject.StringCol(default=None)
    text = sqlobject.StringCol(default=None)
    channel = sqlobject.StringCol(default=None)
    
    _provisioning = 'provision'
    

def get_latest_pastes(number):
    """Get the latest set of pastes up to arg"""
    latest_pastes = []
    paste_length = Paste.select().count()
    if paste_length > number:
        for index in range(number):
            paste = Paste.get(paste_length - index)
            paste_dict = {}
            for attribute in [(attr, getattr(paste, attr)) for attr in dir(paste) if not attr.startswith('_')]:
                try:
                    paste_dict[attribute[0]] = unicode(str(attribute[1]), 'utf-8', 'ignore')
                except:
                    pass
            latest_pastes.append(paste_dict)
        return latest_pastes
    else:
        for index in range(paste_length):
            paste = Paste.get(paste_length - index)
            paste_dict = {}
            for attribute in [(attr, getattr(paste, attr)) for attr in dir(paste) if not attr.startswith('_')]:
                try:
                    paste_dict[attribute[0]] = unicode(str(attribute[1]), 'utf-8', 'ignore')
                except:
                    pass
            latest_pastes.append(paste_dict)
        return latest_pastes

def show_pasting(environ, start_response, paste_id):
    """Show a specific pasting by id"""
    paste = Paste.get(int(paste_id))
    if paste.title is None:
        paste.title = ''
    
    if paste.language == '' or paste.language is None:
        try:
            lexer = pygments.lexers.guess_lexer(unicode(paste.text, 'utf-8', 'ignore'))
        except:
            template = mako_lookup.get_template('show.html')
            start_response('200 Ok', [('Content-Type', 'text/html')])
            return [ template.render(latest_pastes=get_latest_pastes(10),
                              base_url=twisted_soup.settings['web_baseuri'],
                              text=unicode('<h1>'+paste.title+'</h1>'+ '<br>'.join(paste.text.splitlines()), 'utf-8', 'ignore') )]
    else:
        lexer = pygments.lexers.find_lexer_class(paste.language)()
        
    paste_text = highlight(unicode(paste.text, 'utf-8', 'ignore'), lexer, HtmlFormatter(full=True, linenos='table', title=unicode(paste.title, 'utf-8', 'ignore')))
    if type(paste_text) is not unicode:
        paste_text = unicode(paste_text, 'utf-8', 'ignore')
    
    template = mako_lookup.get_template('show.html')
    start_response('200 Ok', [('Content-Type', 'text/html')])
    return [ template.render(latest_pastes=get_latest_pastes(10),
                             base_url=twisted_soup.settings['web_baseuri'],
                             text=paste_text ) ]
                             
def search_pastings(environ, start_response):
    if environ['REQUEST_METHOD'] == 'GET':
        template = mako_lookup.get_template('search.html')
        start_response('200 Ok', [('Content-Type', 'text/html')])
        return [ template.render(latest_pastes=get_latest_pastes(10),
                                 base_url=twisted_soup.settings['web_baseuri'], 
                                 language_list=language_list,
                                 channel_list=channel_list) ]
    elif environ['REQUEST_METHOD'] == 'POST':
        post = environ['wsgi.post_form'][2]
        post_values = {}
        for value in ['language', 'title', 'channel', 'author', 'text']:
            if post[value].value == '':
                post_values[value] = None
            else:
                post_values[value] = '<h1>'+paste.title+'</h1>'+post[value].value
        
        args = []
        for key, value in post_values.items():
            if (key == 'text' and value is not None) or (key == 'title' and value is not None):
                for word in value.split(' '):
                    args.append('Paste.q.%s.contains("%s")' % (key, word))
            elif value is not None:
                args.append('Paste.q.%s == "%s"' % (key, value))
        
        exec('results = Paste.select(sqlobject.AND(%s))' % ', '.join(args))
        results = list(results)
        template = mako_lookup.get_template('search_results.html')
        start_response('200 Ok', [('Content-Type', 'text/html')])
        return [ template.render(latest_pastes=get_latest_pastes(10),
                                 base_url=twisted_soup.settings['web_baseuri'], 
                                 language_list=language_list,
                                 channel_list=channel_list, 
                                 results=results) ]

def add_pasting(environ, start_response):
    """Add a pasting"""
    assert environ['REQUEST_METHOD'] == 'POST'
    post = environ['wsgi.post_form'][2]
    
    post_values = {}
    
    for value in ['language', 'title', 'channel']:
        if post[value].value == '':
            post_values[value] = None
        else:
            post_values[value] = post[value].value
    
    if post['submission_type'].value == 'Add Pasting':
        paste = Paste(author=post['author'].value, text=post['text'].value, 
                      language=post_values['language'], title=post_values['title'], 
                      channel=post_values['channel'])

        uri = twisted_soup.settings['web_baseuri']+'/paste/show/'+str(paste.id)
        if post_values['channel'] is not None:
            send_message = twisted_soup.soup.send_message_register['irc_send_message']
            send_message.send_message(post_values['channel'], '%s created a new paste | %s | %s' % (
                                      post['author'].value, uri, post_values['title']))
                                      
        start_response('303 See Other', [('Location', uri), ('Content-Type', 'text/html')] )
        return ['<html><body>redirecting to <a href="%s">%s</a></body></html>' % (uri, uri)]
    else:
        if post_values['language'] == '' or post_values['language'] is None:
            try:
                lexer = pygments.lexers.guess_lexer(unicode(post['text'].value, 'utf-8', 'ignore'))
                rendered_text = highlight(unicode(post['text'].value, 'utf-8', 'ignore'), lexer, HtmlFormatter(full=True, linenos='table', title=unicode(post_values['title'], 'utf-8', 'ignore')))
            except:
                rendered_text = post['text'].value
        else:
            lexer = pygments.lexers.find_lexer_class(post_values['language'])()
            rendered_text = highlight(unicode(post['text'].value, 'utf-8', 'ignore'), lexer, HtmlFormatter(full=True, linenos='table', title=unicode(post_values['title'], 'utf-8', 'ignore')))
        
        
        template = mako_lookup.get_template('preview.html')
        response_body = template.render(latest_pastes=get_latest_pastes(10),
                                        base_url=twisted_soup.settings['web_baseuri'], 
                                        language_list=language_list, channel_list=channel_list,
                                        author=post['author'].value, text=post['text'].value,
                                        rendered_text=rendered_text,  
                                        paste_language=post_values['language'], 
                                        title=post_values['title'], 
                                        paste_channel=post_values['channel'])
        start_response('200 Ok', [('Content-Type', 'text/html')])
        return [ response_body ]
        
    
def return_file(environ, start_response, file_name):
    """Static file serving"""
    return fileserver_application(environ, start_response)

def paste(environ, start_response, *args):
    """Base web dispatcher"""
    args = list(args)
    actions = {'add_pasting':add_pasting, 'show':show_pasting, 'search':search_pastings}
    
    if len(args) > 0 and args[-1] != 'index.html':
        if args[0].find('.') is not -1:
            return return_file(environ, start_response, args[0])
        else:
            return actions[args.pop(0)](environ, start_response, *args)
    
    template = mako_lookup.get_template('index.html')
    start_response('200 Ok', [('Content-Type', 'text/html')])
    return [ template.render(latest_pastes=get_latest_pastes(10),
                             base_url=twisted_soup.settings['web_baseuri'], 
                             language_list=language_list,
                             channel_list=channel_list) ]

_wsgi_web_app_ = {'paste':paste}