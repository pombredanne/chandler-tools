# Copyright (c) 2007 Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from twisted.words.protocols.jabber import jid, client, xmlstream
from twisted.words.xish import domish
from twisted.internet import reactor
import twisted_soup

def initialize():
    jabber_id = jid.JID( twisted_soup.settings['jabber_jid']+'/TwistedWords' )
    jabber_server = twisted_soup.settings['jabber_server']

    jabber_factory = client.basicClientFactory(jabber_id, twisted_soup.settings['jabber_password'])
    twisted_soup.soup.factory_register['jabber_factory'] = jabber_factory
    
    jabber_operations = JabberDispatcher(jabber_id)
    twisted_soup.soup.jabber_dispatcher = jabber_operations
    jabber_factory.addBootstrap(xmlstream.STREAM_AUTHD_EVENT, jabber_operations.authd)

    reactor.connectTCP(twisted_soup.settings['jabber_server'], 5222, jabber_factory)
    
class JabberDispatcher(twisted_soup.soup.Dispatcher):
    
    _mapping_name = '_jabber_command_mapping_'
    
    def __init__(self, jabber_id):
        self.me = jabber_id
        twisted_soup.soup.Dispatcher.__init__(self, self.send)
        
    def initOnline(self, xmlstream):
    	print 'Initializing jabber...'

    	for handler in [getattr(self, method) for method in dir(self) if method.startswith('handle_')]:
        	xmlstream.addObserver('/'+handler.__name__.split('handle_')[-1], handler)

        xmlstream.addObserver('/*', self.catchall)
        
        self.send_message = twisted_soup.soup.SendMessage(self.send)
        twisted_soup.soup.add_task(5, self.resync_mapping, 'jabber_irc_resync')


    def authd(self, xmlstream):
    	self.thexmlstream = xmlstream
    	print "we've authd!"
    	print repr(xmlstream)

    	#need to send presence so clients know we're
    	#actually online
    	presence = domish.Element(('jabber:client', 'presence'))
    	presence.addElement('status').addContent('Online')
    	xmlstream.send(presence)

    	self.initOnline(xmlstream)

    def send(self, to, msg):
    	message = domish.Element(('jabber:client','message'))
    	message["to"] = jid.JID(to).full()
    	message["from"] = self.me.full()
    	message["type"] = "chat"
    	message.addElement("body", "jabber:client", msg)

    	self.thexmlstream.send(message)

    def handle_message(self, el):
    	# print 'Got message: %s' % str(el.attributes)
    	from_id = el["from"]

    	body = "empty"
    	for e in el.elements():
    		if e.name == "body":
    			body = unicode(e.__str__())
    			
    	args = body.split()
    	if args[0] == "empty":
    	    return
    	if len(args) is not 0:
        	command = args.pop(0).replace('.', '')
        	if len(args) is not 0:
        	    if args[0].startswith('#'):
        	        channel = str(args.pop(0))
        	    else:
        	        channel = None
        	else:
        	    channel = None
        	
        	class JabberSend(twisted_soup.soup.SendMessage):
        	    def __init__(self, channel, from_id, send_message):
        	        self.channel = channel    
        	        self.from_id = from_id
        	        self.send_message_method = send_message
    	        
        	    def send_message(self, user_stub, msg):
        	        """Send message to IRC is channel is not None and back to jabber"""
        	        if channel is not None:
        	            if twisted_soup.soup.send_message_register.has_key('irc_send_message'):
        	                twisted_soup.soup.send_message_register['irc_send_message'].send_message(self.channel, 'From %s | %s' % (str(from_id).split('/')[0], msg))
        	        self.send_message_method(self.from_id, msg)
    	    
        	self.dispatch(str(from_id), channel, str(command), *args, 
        	              **{'send_message':JabberSend(channel, str(from_id), self.send_message.send_message)})

    def catchall(self, el):
    	#print 'Got something: %s' % el.toXml().encode('utf-8')
    	pass

    def handle_presence(self, presence):
        if presence.hasAttribute("type") and presence["type"]=="subscribe":
            response = domish.Element(('jabber:client','presence'))
            response["to"] = presence["from"]
            response["from"] = self.me.full()
            response["type"] = "subscribed"
            print("responding to presence with: %s" % response.toXml().encode('utf-8'))
            self.thexmlstream.send(response)

