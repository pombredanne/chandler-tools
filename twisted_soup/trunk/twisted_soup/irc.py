# Copyright (c) 2001-2004 Twisted Matrix Laboratories.
#   -This code is originally reworked from IRCBot sample code.
# Copyright (c) 2007 Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import twisted_soup
from twisted.words.protocols import irc
from twisted.python import log
from twisted.internet import reactor, protocol

def initialize():
    """create irc factory and dispatcher"""
    irc_factory = IRCSoupFactory(twisted_soup.settings['irc_channels'])    
    irc_factory.protocol.nickname = twisted_soup.settings['irc_nickname']
    reactor.connectTCP(twisted_soup.settings['irc_server_name'], twisted_soup.settings['irc_server_port'], irc_factory)


class IRCDispatcher(twisted_soup.soup.Dispatcher):
    """Dispatcher for IRC"""
    _mapping_name = ''
    
    def add_command(self, channel, name, method):
        self.channel_commands[channel][name] = method
    def remove_command(self, channel, name):
        self.channel_commands[channel].pop(name)
    
    def extended_mapping(self):
        from twisted_soup import extensions as extensions_module
        for extension in [getattr(extensions_module, ext) for ext in dir(extensions_module) if (
                           hasattr(getattr(extensions_module, ext), '_irc_command_mapping_') is True)]:
            for channel in twisted_soup.settings['irc_channels']:
                if ( twisted_soup.settings['irc_disabled_extensions'].has_key(channel) is False ) or ( 
                     extension.__name__.split('.')[-1] not in twisted_soup.settings['irc_disabled_extensions'][channel] ):
                    for name, method in extension._irc_command_mapping_.items():
                        self.channel_commands[channel][name] = method
    
                    
class IRCSoupClient(irc.IRCClient):
    """A logging IRC bot."""

    nickname = "tw-soup"

    def connectionMade(self):
        irc.IRCClient.connectionMade(self)

        twisted_soup.soup.send_message_register['irc_send_message'] = twisted_soup.soup.SendMessage(self.msg)
        self.dispatcher = IRCDispatcher(twisted_soup.soup.send_message_register['irc_send_message'])

        twisted_soup.soup.irc_dispatcher = self.dispatcher
        twisted_soup.soup.add_task(5, self.dispatcher.resync_mapping, 'irc_dispatcher_resync')
        twisted_soup.start_log_session = self.start_log_session
        twisted_soup.stop_log_session = self.stop_log_session
        self.log_sessions = {}
        twisted_soup.irc_log_sessions = self.log_sessions

    def connectionLost(self, reason):
        irc.IRCClient.connectionLost(self, reason)

    def signedOn(self):
        """Called when bot has succesfully signed on to server."""
        for channel in self.factory.channels:
            self.join(channel)

    def joined(self, channel):
        """This will get called when the bot joins the channel."""
        print("[I have joined %s]" % channel)

    def privmsg(self, user, channel, msg):
        """This will get called when the bot receives a message."""

        if channel == self.nickname:
            channel = user
            
        if ( msg.startswith('.') ) and not (
             msg.startswith('..') ) and ( 
             msg.split(' ')[0].find('/') is -1 ):
            if msg.find(' ') is -1:
                command = msg
                self.dispatcher.dispatch(user, channel, command.replace('.', ''))
            else:
                command, args = msg.split(' ', 1)
                if type(args) is not list:
                    args = [args]
                else:
                    args = args.split(' ')
                self.dispatcher.dispatch(user, channel, command.replace('.', ''), *args)
        else:
            user = user.split('!', 1)[0]
            self.log_channel(channel, "<%s> %s" % (user, msg))

    def log_channel(self, channel, msg):
        if channel in self.log_sessions.keys():
            self.log_sessions[channel].write_line(msg)
    
    def start_log_session(self, channel):
        assert channel not in self.log_sessions.keys()
        self.log_sessions[channel] = twisted_soup.soup.LogSession(channel)
    
    def stop_log_session(self, channel):
        assert channel in self.log_sessions.keys()
        self.log_sessions[channel].stop_session
        return self.log_sessions.pop(channel).pop_lines_string()

    def action(self, user, channel, msg):
        """This will get called when the bot sees someone do an action."""
        # user = user.split('!', 1)[0]
        # log.msg("* %s %s" % (user, msg))
        pass

    def irc_NICK(self, prefix, params):
        """Called when an IRC user changes their nickname."""
        old_nick = prefix.split('!')[0]
        new_nick = params[0]
        log.msg("%s is now known as %s" % (old_nick, new_nick))


class IRCSoupFactory(protocol.ClientFactory):
    """A factory for Soup.

    A new protocol instance will be created each time we connect to the server.
    """

    # the class of the protocol to build when new connection is made
    protocol = IRCSoupClient

    def __init__(self, channels):
        self.channels = channels

    def clientConnectionLost(self, connector, reason):
        """If we get disconnected, reconnect to server."""
        connector.connect()

    def clientConnectionFailed(self, connector, reason):
        print "connection failed:", reason
