# Copyright (c) 2007 Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# twisted imports
from twisted.internet import reactor
import sys, os

sys.path.insert( 0, 
     os.path.abspath ( 
     sys.modules[__name__].__file__ + 
     os.path.sep + os.path.pardir + 
     os.path.sep + os.path.pardir  )
     )

import twisted_soup


if __name__ == '__main__':

    twisted_soup.soup.initialize_soup()
    try:
        reactor.run()
    except KeyboardInterrupt:
        reactor.stop()


