# Copyright (c) 2007 Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import twisted_soup
from twisted.web import client

import time
import re

re_time = re.compile(r'''(\s?|^)
                         (?P<hours>\d\d?)
                         (?P<tsep>:|)
                         (?P<minutes>\d\d)
                         (?:(?P=tsep)(?P<seconds>\d\d(?:[.,]\d+)?))?''', re.IGNORECASE + re.VERBOSE)


def logmark(user, channel, url, send_message, *args):
    """
    Create a bookmark url for the irc web log viewer.
    """
    logChannel = channel
    nowTime    = time.strftime('%H%M')
    nowDate    = time.strftime('%Y%m%d')
    times      = [ nowTime, nowTime ]
    template   = 'http://wiki.osafoundation.org/script/getIrcTranscript.cgi?channel=%s&date=%s&startTime=%s&endTime=%s'
    template2  = 'http://wiki.osafoundation.org/script/getIrcTranscript.cgi?channel=%s&date=%s&startTime=%s'
    flag       = True

    for i in range(0, len(args)):
        s = args[i].strip()
        if s in ['chandler', 'cosmo', 'osaf-qa']:
            logChannel = s
        else:
            m = re_time.search(s)
            if m is not None:
                h = int(m.group('hours'))
                if flag:
                    times[0] = '%02d%s' % (h, m.group('minutes'))
                    times[1] = times[0]
                    flag     = False
                else:
                    times[1] = '%02d%s' % (h, m.group('minutes'))

    if logChannel == '':
        logChannel = 'NONE'

    if times[0] == times[1]:
        s = template2 % (logChannel, nowDate, times[0])
    else:
        s = template % (logChannel, nowDate, times[0], times[1])

    send_message.send_message(channel, s)

_irc_command_mapping_    = { 'mark': logmark }
_jabber_command_mapping_ = { 'mark': logmark }
