# Copyright (c) 2007 Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import twisted_soup
from twisted.web import client
import codecs

given_notice = {}

trac_url_map = twisted_soup.settings['trac_url_map']

def create_tiny_url(user, channel, url, send_message, *args):
    """Create a tinyurl for given url."""
    if len(args) is not 0:
        notes = ' '.join(args)
    else:
        notes = None
    
    def print_tiny_url(response):
        lines = response.splitlines()
        tinyurl = None
        for line in lines:
            if line.find('<input type=hidden name=tinyurl value="') is not -1:
                tinyurl = line.split('type=hidden name=tinyurl value="')[-1].split('"')[0]
        if tinyurl is None:
            print 'creating tiny url failed'
        else:
            if notes is None:
                send_message.send_message(channel, '%s created %s' % (user, tinyurl))
            else:
                send_message.send_message(channel, '%s created %s | %s' % (user, tinyurl, notes))
    
    form = 'url=%s' % url
    post = client.getPage('http://tinyurl.com/create.php', method='POST',
                                 headers={'Content-Type': 'application/x-www-form-urlencoded',
                                          'Content-Length': str(len(form))},
                                 postdata=codecs.encode(form, 'ascii', 'ignore') )
                                 
    post.addCallback(print_tiny_url)
    
    
_jabber_command_mapping_ = {'tinyurl':create_tiny_url}
