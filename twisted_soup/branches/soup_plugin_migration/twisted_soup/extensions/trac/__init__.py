# Copyright (c) 2007 Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import twisted_soup
import feedparser
import codecs
from twisted.web import client
from urlparse import urlparse
from StringIO import StringIO

given_notices = twisted_soup.settings['trac_given_notices']

trac_url_map = twisted_soup.settings['trac_url_map']
    
def show_ticket(user, channel, ticket_number, send_message):
    """Show a trac ticket"""
    base_url = urlparse(trac_url_map[channel])
    
    def process_ticket(response):
        keys, values = [ splitup.split('\t') for splitup in response.splitlines()]
        ticket = dict( [  (keys[index], values[index])  for index in range(len(keys)) ] )
        ticket['url'] = '%s/ticket/%s' % (base_url.geturl(), ticket_number)
        text = 'Windmill Ticket %s | %s %s %s %s | %s | %s' % (ticket['id'], ticket['owner'], ticket['type'], ticket['component'], ticket['status'], ticket['url'], ticket['summary'])
        
        send_message.send_message(channel, text)
    
    client.getPage('%s/ticket/%s?format=tab' % (base_url.geturl(), ticket_number)).addCallback(process_ticket)
        
    
def check_notices(*args):
    """Print notices from trac"""
    for channel in given_notices.keys():
        base_url = urlparse(trac_url_map[channel])
        
        def parse_feed_response(response):
            timeline = feedparser.parse(StringIO(response))
    
            new_notices = [ entry for entry in timeline.entries if entry['id'] not in given_notices[channel] ]
            if len(new_notices) is not 0:
                new_notices.reverse()
                output = []
                for notice in new_notices:
                    if notice['link'].find('/wiki/') is not -1:
                        if notice['summary'].find('version=') is not -1:
                            rev = notice['summary'].split('version=')[-1].split('"')[0]
                        else:
                            rev = '1'
                        notice['link'] = '%s?version=%s' % (notice['link'], rev)
                    output.append ( codecs.encode(u'Notice: %s | %s | %s ' % (notice['title'], 
                                                                notice['author'],
                                                                notice['link'] ), 'ascii', 'ignore' ) )
                    given_notices[channel].append(notice['id'])
                    if twisted_soup.soup.send_message_register.has_key('irc_send_message'):
                        twisted_soup.soup.send_message_register['irc_send_message'].breakup_list_and_send(channel, output)
        path = '%s/timeline?milestone=on&ticket=on&changeset=on&wiki=on&max=50&daysback=1&format=rss' % base_url.geturl()
        timeline = client.getPage(path).addCallback(parse_feed_response)
    
_irc_command_mapping_ = {'ticket':show_ticket}
_scheduled_tasks_ = {'check_notices':(60, check_notices)}
