# Copyright (c) 2007 Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import twisted_soup
import codecs
from twisted.web import client
from urlparse import urlparse
import datetime
import sqlobject

class BugBashArchive(sqlobject.SQLObject):
    bugbash_registry = sqlobject.PickleCol()
    verified_registry = sqlobject.PickleCol()
    platform_declarations = sqlobject.PickleCol()
    channel = sqlobject.StringCol()
    end_time = sqlobject.StringCol()

given_notices = twisted_soup.settings['bugzilla_given_notices']

bugbash_registry = {}
verified_registry = {}
platform_declarations = {}

def take_bug(user, channel, ticket_number, send_message):
    """Claim ownership over verifying a bug"""
    # Make sure the user is in the bugbash registry
    if not bugbash_registry[channel].has_key(user):
        bugbash_registry[channel][user] = []
        verified_registry[channel][user] = []
    
    # If someone has already taken or verified the bug then deny them, if not give it to them.
    bugs = []
    for bug_list in bugbash_registry[channel].values(): bugs.extend(bug_list)
    for bug_list in verified_registry[channel].values(): bugs.extend(bug_list)
    if ticket_number in bugs:
        send_message.send_message(channel, 'sorry %s, %s is already taken' % (user.split('!')[0], ticket_number))
    else:
        bugbash_registry[channel][user].append(ticket_number)
        send_message.send_message(channel, '%s has taken %s' % (user.split('!')[0], ticket_number))

def untake_bug(user, channel, ticket_number, send_message):
    """Get rid of ownership of a bug"""
    # Make sure the user is in the bugbash registry
    if not bugbash_registry[channel].has_key(user):
        bugbash_registry[channel][user] = []
        verified_registry[channel][user] = []

    if ticket_number not in bugbash_registry[channel][user]:
        send_message.send_message(channel, 'sorry %s, %s is not owned by you' % (user.split('!')[0], ticket_number))
    else:
        bugbash_registry[channel][user].remove(ticket_number)
        send_message.send_message(channel, '%s has given up on %s' % (user.split('!')[0], ticket_number))
    
def verified_bug(user, channel, ticket_number, send_message):
    """Claim you've verified a bug"""
    if bugbash_registry[channel].has_key(user) and ticket_number in bugbash_registry[channel][user]:
        bugbash_registry[channel][user].remove(ticket_number)
        verified_registry[channel][user].append(ticket_number)
        send_message.send_message(channel, '%s has verified %s' % (user.split('!')[0], ticket_number))
    else:
        send_message.send_message(channel, 'sorry %s, you haven\'t taken %s' % (user.split('!')[0], ticket_number))
        
def declare_platform(user, channel, *args, **kwargs):
    """Declare a platform you are verifying bugs on"""
    send_message = kwargs['send_message']
    if user not in platform_declarations[channel]:
        platform_declarations[channel][user] = []
    platform_declarations[channel][user].append(' '.join(args))
    send_message.send_message(channel, '%s is on %s' % (user.split('!')[0], ' '.join(args)))

def start_bugbash(user, channel, send_message):
    """Start a 'bugbash', a bug verifying fest."""
    bugbash_registry[channel] = {}
    verified_registry[channel] = {}
    platform_declarations[channel] = {}
    twisted_soup.soup.irc_dispatcher.add_command(channel, 'take', take_bug)
    twisted_soup.soup.irc_dispatcher.add_command(channel, 'verify', verified_bug)
    twisted_soup.soup.irc_dispatcher.add_command(channel, 'untake', untake_bug)
    twisted_soup.soup.irc_dispatcher.add_command(channel, 'platform', declare_platform)
    twisted_soup.soup.irc_dispatcher.add_command(channel, 'stop_bugbash', stop_bugbash) 
    send_message.send_message(channel, 'starting bugbash %s/bugzilla/bugbash/%s' % 
                                       (twisted_soup.settings['web_baseuri'], 
                                        channel.replace('#', '')) )
    
def stop_bugbash(user, channel, send_message):
    """Stop the current bugbash."""
    bug_count = 0
    for user, bug_list in verified_registry[channel].items():
        bug_count += len(bug_list)
    
    end_time = datetime.datetime.now().strftime('%d-%b-%Y-%H-%M-%S')
    BugBashArchive(bugbash_registry=bugbash_registry.pop(channel),
                   verified_registry=verified_registry.pop(channel),
                   platform_declarations=platform_declarations.pop(channel),
                   end_time=end_time, channel=channel)
    twisted_soup.soup.irc_dispatcher.remove_command(channel, 'take')
    twisted_soup.soup.irc_dispatcher.remove_command(channel, 'verify') 
    twisted_soup.soup.irc_dispatcher.remove_command(channel, 'untake')
    twisted_soup.soup.irc_dispatcher.remove_command(channel, 'platform') 
    twisted_soup.soup.irc_dispatcher.remove_command(channel, 'stop_bugbash')
    send_message.send_message(channel, 'bugbash has ended, %s bugs verified, you can see the archive at %s/bugzilla/bugbash/%s/%s' % (str(bug_count), twisted_soup.settings['web_baseuri'], channel.replace('#',''), end_time ))
    
def bugbash_web(environ, start_response, channel, bugbash_registry, verified_registry, platform_declarations):
    body = '<h2>Platforms</h2>'
    for user, platforms in sorted(platform_declarations.items()):
        platforms_string = ', '.join(platforms)
        body += '%s is on %s' % (user.split('!')[0], platforms_string   )
    body += '<h2>Taken bugs</h2>'
    for user, bugs in sorted(bugbash_registry.items()):
        bug_strings = []
        for bug in bugs:
            bug_strings.append('<a href="%s/show_bug.cgi?id=%s">%s</a>' % (
                                twisted_soup.settings['bugzilla_url_map'][channel], bug, bug) )
        body += '%s has : %s<br>' % (user.split('!')[0], ', '.join(bug_strings)) 
    body += '<h2>Verified bugs</h2>'
    for user, bugs in sorted(verified_registry.items()):
        bug_strings = []
        for bug in bugs:
            bug_strings.append('<a href="%s/show_bug.cgi?id=%s">%s</a>' % (
                                twisted_soup.settings['bugzilla_url_map'][channel], bug, bug) )
        body += '%s verified : %s<br>' % (user.split('!')[0], ', '.join(bug_strings)) 
    start_response('200 Ok', [('Content-Type', 'text/html')])
    return [ '<html><body>'+body+'</body></html>' ]
        
def bugbash_web_chooser(environ, start_response, channel, date=None):
    channel = '#'+channel
    if date is None:
        if channel not in bugbash_registry.keys():
            start_response('200 Ok', [('Content-Type', 'text/html')])
            return [ '<html><body>There is currently no bugbash open for this channel</body></html>' ]
        return bugbash_web(environ, start_response, channel, 
                           bugbash_registry=bugbash_registry[channel],
                           verified_registry=verified_registry[channel],
                           platform_declarations=platform_declarations[channel])
    else:
        archive = list(BugBashArchive.selectBy(channel=channel, end_time=date))[0]
        return bugbash_web(environ, start_response, channel, 
                           bugbash_registry=archive.bugbash_registry,
                           verified_registry=archive.verified_registry,
                           platform_declarations=archive.platform_declarations)
    
def web_application(environ, start_response, *args):
    args = list(args)
    dispatcher = {'bugbash': bugbash_web_chooser}
    
    method = dispatcher[args.pop(0)]
    return method(environ, start_response, *args)
    
_irc_command_mapping_ = {'bugzilla_bugbash':start_bugbash}
_wsgi_web_app_ = {'bugzilla':web_application}

    
