# Copyright (c) 2007 Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# IRC settings
irc_nickname = None
irc_server_name = 'irc.freenode.org'
irc_server_port = 6667
irc_channels = ['#nothing', 
                #'#osaf-qa',
                ]

irc_disabled_extensions = {'#windmill': ['bugzilla'],
                           #'#osaf-qa': ['trac']
                           }

# Jabber Settings
jabber_server = 'jabber.org'
jabber_jid = None
jabber_password = None

# Trac plugin settings
trac_given_notices = {'#nothing':[]}
trac_url_map = {'#nothing':'http://windmill.osafoundation.org/trac',
                #'#osaf-qa':'http://windmill.osafoundation.org/trac',
                }

# Bugzilla plugin settings
bugzilla_given_notices = []
bugzilla_url_map = {'#nothing':'http://bugzilla.osafoundation.org',
                    #'#osaf-qa':'http://bugzilla.osafoundation.org',
                    }

# Misc.
web_port = 8081
web_baseuri = 'http://localhost:8081'
sqlite_db = 'sql_soup.db'

