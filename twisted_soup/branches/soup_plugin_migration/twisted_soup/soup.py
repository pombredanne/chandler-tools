# Copyright (c) 2007 Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from twisted.internet import reactor, protocol
from twisted.python import log
from twisted.words.xish import domish
import copy, time, sys, os, types, random
from twisted.application.internet import TimerService

import twisted_soup

factory_register = {}
send_message_register = {}
tasks = []
store = {}

log_sessions = {}

class LogSession(object):
    """Object for holding a log of lines from a single channel"""
    def __init__(self, channel):
        self.channel = channel
        self.lines = []
        log_sessions[channel] = self
    def write_line(self, line):
        self.lines.append(line)
    def pop_lines_list(self):
        return_list = copy.copy(self.lines)
        self.lines = []
        return return_list
    def pop_lines_string(self):
        return_string = '\n'.join(self.lines)
        self.lines = []
        return return_string
    def close_session(self):
        log_session.remove(self.channel)

def start_log_session(channel):
    # Don't forget to add conveinience methods
    pass

def add_task(timing, method, name):
    task = TimerService(timing, method)
    if task not in tasks:    
        task.setName(name)
        task.startService()

def breakup_text(text, remove=[]):
    
    blank_lines = ['\r', '\r\r', '\r\r\r']
    remove.extend(blank_lines)
    
    initial_set = text.splitlines()
    while '' in initial_set:
        initial_set.remove('')
        
    for removal in remove:
        while removal in initial_set:
            initial_set.remove(removal)
        
    return initial_set

def encode_form(inputs):
    """
    Takes a dict of inputs and returns a multipart/form-data string
    containing the utf-8 encoded data. Keys must be strings, values
    can be either strings or file-like objects.
    """
    getRandomChar = lambda: chr(random.choice(range(97, 123)))
    randomChars = [getRandomChar( ) for x in range(20)]
    boundary = "---%s---" % ''.join(randomChars)
    lines = [boundary]
    for key, val in inputs.items( ):
        header = 'Content-Disposition: form-data; name="%s"' % key
        if hasattr(val, 'name'):
            header += '; filename="%s"' % os.path.split(val.name)[1]
        lines.append(header)
        if hasattr(val, 'read'):
            lines.append(val.read( ))
        else:
            lines.append(val.encode('utf-8'))
        lines.append('')
        lines.append(boundary)
    return "\r\n".join(lines)
    
def initialize_models(): 
    """Create the persistent store and make sure all extension defined objects are there"""
    import sqlobject
    
    sys.path.append(os.path.abspath(os.path.dirname(sys.modules[__name__].__file__)+'/'+os.path.pardir))
    
    db_filename = os.path.abspath(twisted_soup.settings['sqlite_db'])
    if os.path.lexists(db_filename):
        provisioned = True
    
    connection = sqlobject.connectionForURI('sqlite://%s' % db_filename)
    sqlobject.sqlhub.processConnection = connection
    
    from twisted_soup import extensions
    
    for module in [getattr(extensions, module_name) for module_name in dir(extensions)]:
        for class_def in [getattr(module, attribute) for attribute in dir(module) if (
                          type(getattr(module, attribute)) is sqlobject.declarative.DeclarativeMeta )]:
            if not class_def.tableExists():
                class_def.createTable()
                if hasattr(class_def, '_provisioning'):
                    getattr(module, class_def._provisioning)()
    
def initialize_soup():
    import irc, jabber, web
    import simplesettings
    
    log.startLogging(sys.stdout)
    
    print 'Initializing settings'
    simplesettings.initialize_settings(twisted_soup.global_settings, main_module=twisted_soup, 
                                       local_env_variable='TWISTED_SOUP_SETTINGS')                
        
    if twisted_soup.settings['sqlite_db'] is not None:
        initialize_models()
    if twisted_soup.settings['irc_nickname'] is not None:
        irc.initialize()
    if twisted_soup.settings['jabber_jid'] is not None:
        jabber.initialize()
    if twisted_soup.settings['web_port'] is not None:
        web.initialize()

    from twisted_soup import extensions
    for extension in [getattr(extensions, ext) for ext in dir(extensions) if (
                       hasattr(getattr(extensions, ext), '_scheduled_tasks_') is True)]:
        for name, value in extension._scheduled_tasks_.items():
            add_task(value[0], value[1], name)

class Dispatcher(object):
    
    _mapping_name = None
    
    def __init__(self, send_message):
        self.send_message = send_message
        self.configure_mapping()
    
    def configure_mapping(self):
        self.channel_commands = {}
        for channel in twisted_soup.settings['irc_channels']:
            self.channel_commands[channel] = {}
        
        from twisted_soup import extensions as extensions_module
        self.extensions = [getattr(extensions_module, ext) for ext in dir(extensions_module) if (
                           hasattr(getattr(extensions_module, ext), self._mapping_name) is True) ]
        
        self.command_mapping = {'help': self.help_command }
        for extension in self.extensions:
            if hasattr(extension, self._mapping_name):
                self.command_mapping.update(getattr(extension, self._mapping_name))
                
        
        if hasattr(self, 'extended_mapping'):
            self.extended_mapping()
        self.build_synctable()
        
    def build_synctable(self):
        self.extension_synctable = {}
        for extension in self.extensions:
            self.extension_synctable[extension.__name__.split('.')[-1]] = open(extension.__file__.replace('.pyc', '.py'), 'r').read()
            
    def resync_mapping(self, user=None, channel=None, message=None):
        """Resync all the source files in the extensions that have changed  """
        from twisted_soup import extensions as extensions_module
        resyncs = {}
        for extension, extension_source_string in self.extension_synctable.items():
            if ( open(getattr(extensions_module, extension).__file__.replace('.pyc', '.py'), 'r').read() != 
                 extension_source_string ):
                resyncs[extension] = getattr(extensions_module, extension)
        
        if len(resyncs) is not 0:
            try:
                for sync in resyncs.values():
                    reload(sync)
            except Exception, e:
                if channel is not None:
                    self.send_message(channel, 'Resync failed on import of %s :: invalid syntax %s: %s' % (sync.__name__, e.__class__.__name__, e))
                return
            for sync in resyncs.values():
                setattr(extensions_module, sync.__name__, reload(sync))
                sys.modules[sync.__name__] = reload(sync)
            self.configure_mapping()
            if channel is not None:
                self.send_message(channel, 'Resynced extensions %s' % str(resyncs))
        else:
            if channel is not None:
                self.send_message(channel, 'No changes to any existing extension source files')
                
    def force_resync(self, user=None, channel=None, message=None):
        """Force resync all extension modules"""
        for extension in dir(extensions_module):
            if type(getattr(extensions_module, extension)) is types.ModuleType:
                setattr(extensions_module, extension, reload(getattr(extensions_module, extension)) )
        self.configure_mapping()
        
    def dispatch(self, user, channel, command, *args, **kwargs):
        if not kwargs.has_key('send_message'):
            kwargs['send_message'] = self.send_message
        
        if ( self.channel_commands.has_key(channel) ) and ( self.channel_commands[channel].has_key(command) ):
            try:
                self.channel_commands[channel][command](user, channel, *args, **kwargs)
            except:
                log.err()
                if not kwargs.has_key('send_message'):
                    self.send_message.send_message(channel, '%s from %s resulted in error, see log for traceback' % (command, user))
                else:
                    kwargs['send_message'].send_message(channel, '%s from %s resulted in error, see log for traceback' % (command, user))
        elif self.command_mapping.has_key(command):   
            try: 
                self.command_mapping[command](user, channel, *args, **kwargs)
            except:
                log.err()
                if not kwargs.has_key('send_message'):
                    self.send_message.send_message(channel, '%s from %s resulted in error, see log for traceback' % (command, user))
                else:
                    kwargs['send_message'].send_message(channel, '%s from %s resulted in error, see log for traceback' % (command, user))
        else:
            if not kwargs.has_key('send_message'):
                self.send_message.send_message(channel, 'Don\'t have command named "%s"' % command)
            else:
                kwargs['send_message'].send_message(channel, 'Don\'t have command named "%s"' % command)
        
                                
    def help_command(self, user, channel, send_message, *args):
        """Display usage for all available commands"""
        message = '== Commands and Usage ==\n'
        commands = self.command_mapping.items()
        if self.channel_commands.has_key(channel):
            commands.extend( self.channel_commands[channel].items() )
        commands.sort()
        for command in commands:
            description_string = '\n        '.join(command[1].__doc__.splitlines())  
            message += '   .%s  ::  %s\n' % (command[0], description_string)
        send_message.breakup_and_send(channel, message)

class SendMessage(object):
    
    def __init__(self, send_message_method):
        self.send_message_method = send_message_method
        self.send_message = send_message_method
        
    def breakup_and_send(self, destination, message):
        self.breakup_list_and_send(destination, breakup_text(message))

    def breakup_list_and_send(self, destination, message_list):
        for line in message_list:
            reactor.callLater(message_list.index(line), self.send_message, *(destination, line))


#  * http://twistedmatrix.com/pipermail/twisted-python/2004-May/007903.html
#  * http://redfoot.net/hypercode/modules/xmpp/client



