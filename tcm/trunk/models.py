#   Copyright (c) 2006-2007 Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import sqlobject
import sys, os


class TestCase(sqlobject.SQLObject):
    # Table attributes
    name = sqlobject.StringCol()
    description = sqlobject.StringCol()
    comment = sqlobject.StringCol(default=None)
    result = sqlobject.StringCol()
    url = sqlobject.StringCol(default=None)
    suite = sqlobject.ForeignKey('TestSuite')
    coverage_level = sqlobject.ForeignKey('CoverageLevel')
    
    # Foreign Joins
    references = sqlobject.RelatedJoin('Reference')
    
class TestSuite(sqlobject.SQLObject):
    # Table attributes
    name = sqlobject.StringCol()
    description = sqlobject.StringCol()
    comment = sqlobject.StringCol(default=None)
    parent_suite = sqlobject.ForeignKey('TestSuite', default=None)
    parent_plan = sqlobject.ForeignKey('TestPlan')
    
    # Foreign Joins
    references = sqlobject.RelatedJoin('Reference')
    tests = sqlobject.MultipleJoin('TestCase')
    child_suites = sqlobject.MultipleJoin('TestSuite')
    
class TestPlan(sqlobject.SQLObject):
    # Table attributes
    name = sqlobject.StringCol()
    description = sqlobject.StringCol()
    purpose = sqlobject.StringCol()
    revision = sqlobject.FloatCol()
    
    owner = sqlobject.ForeignKey('User')
    categories = sqlobject.RelatedJoin('Category')
    product = sqlobject.ForeignKey('ProductVersion')
    
    # Foreign Joins
    suites = sqlobject.MultipleJoin('TestSuite')
    
class Category(sqlobject.SQLObject):
    # Table attributes
    name = sqlobject.StringCol()
    description = sqlobject.StringCol()
    
    test_plans = sqlobject.RelatedJoin('TestPlan')
    
class Product(sqlobject.SQLObject):
    # Table attributes
    name = sqlobject.StringCol()
    description = sqlobject.StringCol()
    
    # Foreign Joins
    versions = sqlobject.MultipleJoin('ProductVersion')
    
class ProductVersion(sqlobject.SQLObject):
    # Table attributes
    name = sqlobject.StringCol()
    revision = sqlobject.StringCol()
    description = sqlobject.StringCol()
    product = sqlobject.ForeignKey('Product')
    
    # Foreign Joins
    test_plans = sqlobject.MultipleJoin('TestPlan')

class CoverageLevel(sqlobject.SQLObject):
    # Table attributes
    name = sqlobject.StringCol()
    int_rep = sqlobject.IntCol()
    

class Reference(sqlobject.SQLObject):
    # Table attributes
    name = sqlobject.StringCol()
    description = sqlobject.StringCol()
    url = sqlobject.StringCol()
    
    # Foreign Joins
    tests = sqlobject.RelatedJoin('TestCase')
    suites = sqlobject.RelatedJoin('TestSuite')
    
class User(sqlobject.SQLObject):
    # Table attributes
    username = sqlobject.StringCol()
    password = sqlobject.StringCol()
    
    test_plans = sqlobject.MultipleJoin('TestPlan')
    

def provision(url='sqlite:/:memory:'):

    connection = sqlobject.connectionForURI(url)
    sqlobject.sqlhub.processConnection = connection
    
    TestCase.createTable()
    TestSuite.createTable()
    TestPlan.createTable()
    Category.createTable()
    Product.createTable()
    ProductVersion.createTable()
    CoverageLevel.createTable()
    Reference.createTable()
    User.createTable()
    
    anonymous = User(username='anonymous', password=None)
    reference = Reference(name='Testing References', description='testing refernces description',
                          url='http://www.example.com')
    acceptance_coverage = CoverageLevel(name='Acceptance', int_rep=0)
    release_coverage = CoverageLevel(name='Release', int_rep=1)
    exhaustive_coverage = CoverageLevel(name='Exhaustive', int_rep=2)
    chandler_product = Product(name='Chandler', description='A super badass PIM')
    chandler_version_alpha5 = ProductVersion(name='0.7alpha5', description='our 0.7a5 release', revision='0.7.05',
                                             product=chandler_product)
    email_category = Category(name='Email', description='email category description')
    test_plan = TestPlan(name='TestingPlan', description='test description', purpose='to test',
                         revision=0.001, owner=anonymous, product=chandler_product)
    test_suite_one = TestSuite(name='TestingSuite', description='test suite description', parent_plan=test_plan)
    test_sub_suite = TestSuite(name='TestSubSuiteStuff', description='testing subsuites', parent_plan=test_plan,
                               parent_suite=test_suite_one)
    test_case = TestCase(name='TestCaseOne', description='test case description', result='test result',
                         suite=test_sub_suite, coverage_level=release_coverage)
                         
    return [anonymous, reference, acceptance_coverage, release_coverage, exhaustive_coverage, chandler_product,
            chandler_version_alpha5, email_category, test_plan, test_suite_one, test_sub_suite, test_case]
    
    
    
    
    
    
    
    
    
    