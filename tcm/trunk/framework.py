#   Copyright (c) 2006-2007 Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

import urlparse

def reconstruct_url(environ):
    # From WSGI spec, PEP 333
    from urllib import quote
    url = environ['wsgi.url_scheme']+'://'

    if environ.get('HTTP_HOST'):
        url += environ['HTTP_HOST']
    else:
        url += environ['SERVER_NAME']

        if environ['wsgi.url_scheme'] == 'https':
            if environ['SERVER_PORT'] != '443':
               url += ':' + environ['SERVER_PORT']
        else:
            if environ['SERVER_PORT'] != '80':
               url += ':' + environ['SERVER_PORT']

    url += quote(environ.get('SCRIPT_NAME',''))
    url += quote(environ.get('PATH_INFO',''))
    if environ.get('QUERY_STRING'):
        url += '?' + environ['QUERY_STRING']
    
    environ['reconstructed_url'] = url
        
    return url


class ChooserApplication(object):
    
    def __init__(self, starts_with, ends_with, fallback):
        
        self.starts_with = starts_with
        self.ends_with = ends_with
        self.fallback = fallback
        
    def __call__(self, environ, start_response):
        
        url = urlparse.urlpase(reconstruct_url(environ))
        environ['start_response'] = start_response
        args = url.path.split('/')
        
        if args[0] in set(self.starts_with.keys()):
            key = args.pop[0]
            return self.starts_with[key](environ, *args)
            
        if args[-1] in set(self.ends_with.keys()):
            key = args.pop[-1]
            return self.ends_with[key](environ, *args)
            
        args = url.path.split('/')
        return self.fallback(environ, *args)
        
        
from wsgi.simple_server import make_server, WSGIServer, WSGIRequestHandler

def create_server(host, port, fallback, starts_with={}, ends_with={}, server_class=WSGIServer, server_handler=WSGIRequestHandler):
    
    app = ChooserApplication(starts_with, ends_with, fallback)
    return make_server(host, port, app, server_class, server_handler)

    













