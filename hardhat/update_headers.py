#!/usr/bin/env python
#   Copyright (c) 2007 Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

"""
"""

# TODO process html, c and other files for copyright updates


import os, sys
import glob
import time
import re
from optparse import OptionParser

excludes = ['release', 'debug', '.svn', 'test_profile']

reYearRange = re.compile('\-\s*200\d')
reYear      = re.compile('\s+\d\d\d\d\s+')


def checkLine(line):
    """
    >>> data = ['#   Copyright (c) 2006 Open Source Applications Foundation',
    ...         '#   Copyright (c) 2003-2006 Open Source Applications Foundation',
    ...         '#   Copyright (c) 2004-2006 Open Source Applications Foundation',
    ...        ]
    >>> for line in data:
    ...     print checkLine(line)
    (17, 23, False)
    (22, 27, True)
    (22, 27, True)
    
    >>> data = ['#   Copyright (c) 2003-2007 Open Source Applications Foundation',
    ...         '#   Copyright (c) 2007 Open Source Applications Foundation',
    ...        ]
    >>> for line in data:
    ...     print checkLine(line)
    None
    None
    """
    result = None
    s      = line.lower()

    if 'copyright' in s and 'open source application' in s and options.sYear not in s:
        s = line[:-1]
        m = reYearRange.search(s)
        if m is not None:
            result = (m.start(), m.end(), True)
        else:
            m = reYear.search(s)
            if m is not None:
                result = (m.start(), m.end(), False)

    return result

def checkFile(filename):
    fileinfo = os.stat(filename)

    lastModified = time.localtime(fileinfo[8])

    if lastModified[0] == options.year:
        data   = open(filename).read()
        target = None

        for line in data.split('\n'):
            pos = checkLine(line)
            if pos is not None:
                target = (pos, line)
                break

        if target is None:
            print 'File modified in %s but no copyright statement found [%s]' % (options.sYear, filename)
        else:
            start   = target[0][0]
            end     = target[0][1]
            isRange = target[0][2]
            old     = target[1]

            if isRange:
                new = '%s-%s%s' % (old[:start], options.sYear, old[end:])
            else:
                new = '%s-%s %s' % (old[:end - 1], options.sYear, old[end:])

            lines = data.split(old, 1)

            f = open(filename, 'w')
            f.write(lines[0])
            f.write(new)
            f.write(lines[1])
            f.close()

            print 'Copyright statement updated [%s]' % filename

            # print '----------------'
            # print 'match found', filename
            # print start, end
            # print old
            # print '%s%s' % (' ' * start, '^' * (end - start))
            # print new

def scanFiles(scanDir):
    for filename in glob.glob(os.path.join(scanDir, '*.py')):
        if os.path.isfile(filename):
            checkFile(filename)

    for dirname in os.listdir(scanDir):
        fullname = os.path.join(scanDir, dirname)

        if os.path.isdir(fullname) and dirname not in excludes:
            scanFiles(fullname)

def checkOptions():
    usage  = "usage: %prog [options]\n"
    parser = OptionParser(usage=usage, version="%prog")

    parser.add_option('-y', '--year',     dest='year',     default=time.strftime('%Y'), type='int', help='copyright year to update to')
    parser.add_option('-p', '--path',     dest='path',     default=os.getcwd(), help='where to find the chandler source files')
    parser.add_option('-t', '--selftest', dest='selftest', default=False, action='store_true', help='run self tests and exit')

    (options, args) = parser.parse_args()
    options.args    = args

    options.path  = os.path.abspath(options.path)
    options.sYear = str(options.year)

    return options

if __name__ == '__main__':
    global options

    options = checkOptions()

    if options.selftest:
        import doctest

        (failed_count, test_count) = doctest.testmod(optionflags=doctest.ELLIPSIS | doctest.REPORT_NDIFF)

        if failed_count != 0:
            sys.exit(1)
        else:
            sys.exit(0)

    if os.path.exists(os.path.join(options.path, 'Chandler.py')):
        scanFiles(options.path)
    else:
        print 'Unable to locate chandler at [%s]' % options.path

    