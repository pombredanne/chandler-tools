#!/usr/bin/env python

#   Copyright (c) 2004-2007 Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# Author(s): Heikki Toivonen (heikki@osafoundation.org)

# 29 April 2005 - modified by bear (bear@osafoundation.org) to use the new paths

import os, glob, sha, re, shutil, sys

debug = False # Print debug info
dryrun = False # The real thing except we will not acquire lock and will not copy

"""
  Assumed directory layout where to look for files:
 
  external
    staging
      windows
        timestamp(s)
      macosx
        timestamp(s)
        10.5
          timestamp(s)
      maciosx
        timestamp(s)
        10.5
          timestamp(s)
      linux
        timestamp(s)
        gutsy
          timestamp(s)
        feisty
          timestamp(s)
 
  The files should be copied into:
 
  external
    windows
    macosx
      10.5
    maciosx
      10.5
    linux
        gutsy
        feisty
 
  It is assumed that timestamp is of the form yyyymmddhhmmss. It is assumed
  that a timestamp dir that has been completely uploaded to, will have file
  called completed in it. It is further assumed that files in the timestamp
  dir are of the form:
    <packagename>-<debug|release>-<relver>.tar.gz<empty or .md5>
  and that for each release version there is a corresponding debug version,
  and that for each *.tar.gz file there is also a corresponding *.md5 file.
  Platforms are also always expected to be in sync, so only packages that
  are available for all platforms will be available for copy. Files will not
  be overwritten.

  This module will import module called pw, which is expected to have the
  login-password dictionary:

  db = {'somelogin' : 'somepasswordhash'}

  where 'somepasswordhash' is computed as follows:

    import sha
    s = sha.new()
    s.update('somelogin' + 'somepassword')
    hash = s.hexdigest()
    print "'%s': '%s'" % (somelogin, hash)
"""

rootDir = '/home/builder/www/docs/external'
stagingRootDir = rootDir + '/staging'
destRootDir = rootDir

rootUrl = '/external'
stagingRootUrl = rootUrl + '/staging'
destRootUrl = rootUrl

lockFile = destRootDir + '/windows/lock'

haveLock = False

allPlatforms = {'windows': ('.',),
                'macosx':  ('.', '10.5'),
                'linux':   ('.', 'feisty', 'gutsy'),
                'maciosx': ('.', '10.5'),
                }

class PlatformTarballs(object):
    def __init__(self, plat, subPlatform, timestamp=''):
        """
        @param plat: 'windows', 'macosx', ...
        @param subPlatform: '.', '10.5', ...
        @param timestamp: 'yyyymmddhhmmss'
        @param tarballs: ['foo.tar.gz', ...]
        """
        self.platform = plat
        self.subPlatform = subPlatform
        self.timestamp = timestamp
        self.tarballs = []

def latestBinaryTarballs():
    """
    Go through all platform staging areas and collect the binary tarballs
    from the latest staging directory.
    
    @return: List of PlatformTarballs
    """
    ret = []
    keys = allPlatforms.keys()
    keys.sort()
    for key in keys:
        platformRoot = os.path.join(stagingRootDir, key)
        targetRoot = os.path.join(destRootDir, key)

        for sub in allPlatforms[key]:
            if sub != '.':
                plat = os.path.join(platformRoot, sub)
            else:
                plat = platformRoot
            
            pts = PlatformTarballs(key, sub)
            ret.append(pts)

            timestamps = glob.glob(plat + '/[0-9]*')
            if not timestamps:
                continue
            
            timestamps.sort()    # sort first
            timestamps.reverse() # go through them newest to oldest
            timestamp = None
            for t in timestamps:
                t = os.path.basename(t)
                if os.path.isfile(os.path.join(plat, t, 'completed')):
                    timestamp = t
                    break
            if timestamp is None:
                continue
            
            pts.timestamp = timestamp
            
            tarballs = []
            for tarball in glob.glob(os.path.join(plat, timestamp) + '/*.tar.gz'):
                tarballs.append(os.path.basename(tarball))
            tarballs.sort()
            
            # Remove any tarballs that already exist in target dir; this tool
            # does not support overwriting for security reasons.
            if sub != '.':
                target = os.path.join(targetRoot, sub)
            else:
                target = targetRoot
            
            for t in glob.glob(target + '/*.tar.gz'):
                try:
                    tarballs.remove(os.path.basename(t))
                except ValueError:
                    pass
            if not tarballs:
                continue
            
            tarballs.sort()
            pts.tarballs = tarballs            
            
    return ret

def passwordMatched(login, password):
    """
    Primitive password matching function.
    """
    if not login or not password:
        return False
    
    import pw

    shaObj = sha.new()
    shaObj.update(login + password)

    try:
        if pw.db[login] == shaObj.hexdigest():
            return True
    except KeyError:
        pass

    return False

def buildFrontPage():
    """
    This will build the front page the user goes to.
    """
    platTarballs = latestBinaryTarballs()

    if debug:
        print '<p>results:</p>', platTarballs
        
    print '<title>Copy external/internal tarballs</title></head><body>'
    print '<h1>Copy external/internal tarballs</h1>'

    print '<p><a href="%s">[Staging Area]</a>' % stagingRootUrl
    print '<a href="%s">[Destination Area]</a></p>' % destRootUrl

    if not platTarballs:
        print '<p>No tarballs available to copy.</p>'
        return

    print '<form action="%s" method="POST">' % os.path.basename(sys.argv[0])

    for pts in platTarballs:
        print '<p>%s %s %s:</p>' % (pts.platform, pts.subPlatform, pts.timestamp)
        print '<select name="%s_%s_%s" size="5" multiple="1">' % (pts.platform, pts.subPlatform, pts.timestamp)
        for tarball in pts.tarballs:
            print '<option value="%s">%s</option>' % (tarball, tarball)
        print '</select>'        

    print '<p>Login: <input name="login" size="20"></p>'
    print '<p>Password: <input name="password" type="password" size="20"></p>'
    print '<input type="submit" value="Copy selected files">'
    print '<input type="reset">'
    print '</form>'

def copyWanted(form):
    """
    Try to copy the wanted files from staging area to real downloads area.
    """
    print '<title>Copying Tarballs</title></head><body><h1>Copying Tarballs</h1>'
    
    keys = allPlatforms.keys()
    keys.sort()
    
    formKeys = form.keys()
    
    tbPattern = '^[\w\.]+\-(release|debug)\-[\w\.\-]+\.tar\.gz$'
    tbMatcher = re.compile(tbPattern)

    for fk in formKeys:
        if debug:
            print fk
        
        # Sanity check input
        try:
            plat, subPlatform, timestamp = fk.split('_')
        except ValueError:
            continue
        if plat not in keys:
            continue
        if subPlatform not in allPlatforms[plat]:
            continue
        if len(timestamp) != 14:
            continue
        if not timestamp.isdigit():
            continue
        
        tarballs = form.getlist(fk)
        
        platformRoot = os.path.join(stagingRootDir, plat)
        targetRoot = os.path.join(destRootDir, plat)
        
        if subPlatform != '.':
            platDir = os.path.join(platformRoot, subPlatform, timestamp)
            targetDir = os.path.join(targetRoot, subPlatform)
        else:
            platDir = os.path.join(platformRoot, timestamp)
            targetDir = targetRoot

        print '<p><b>%s %s:</b></p>' % (plat, subPlatform)

        for tarball in tarballs:
            # Sanity check input
            match = tbMatcher.match(tarball)
            if not match:
                if debug:
                    print '<p>%s did not pass regex</p>' % tarball
                continue
            
            if debug:
                print '<p>Source: %s<br>Target: %s</p>' % (platDir, targetDir)
            print '<p>copying %s...' % tarball

            if dryrun:
                print 'DRYRUN</p>'
                continue

            targetFile = os.path.join(targetDir, tarball)

            # XXX Any way to get around this race conditions?
            if os.path.exists(targetFile):
                print 'FILE EXISTS, NOT OVERWRITTEN</p>'
                continue
            try:
                shutil.copy(os.path.join(platDir, tarball), targetFile)
            except Exception:
                print 'COPY ERROR</p>'
                continue
            
            print 'ok</p>'

    print '<p>Done!</p>'


def buildSubmitPage(form):
    """
    This will build the page that is given to the user once they submit on
    the front page.
    """
    if not (form.has_key("login") and form.has_key("password")):
        print '<title>Error</title></head><body>'
        print '<h1>Error</h1>'
        print "Please fill in the login and password fields."
        return
    if not passwordMatched(form.getvalue('login'), form.getvalue('password')):
        print '<title>Error</title></head><body>'
        print '<h1>Error</h1>'
        print "Invalid login."
        return

    lock()

    copyWanted(form)

class LockingError(Exception):
    """
    Raised when lock can't be obtained.
    """
    pass

def lock():
    """
    Try to lock the staging area so that only one user at a time can
    do the copy operations. Will raise an exception if lock can't be obtained.
    """
    if not dryrun:
        try:
            os.open(lockFile, os.O_CREAT | os.O_EXCL)
        except OSError, e:
            if debug:
                print e
            raise LockingError, 'can not create lock file'

    global haveLock
    haveLock = True

def unlock():
    """
    Try to unlock if a lock exists. Will raise an exception if lock can't
    be removed. Safe to call even if there is no lock.
    """
    global haveLock
    
    if debug:
        print 'haveLock=', haveLock
        
    if haveLock:
        if not dryrun:
            os.remove(lockFile)

if __name__ == "__main__":
    import cgi

    print 'Content-Type: text/html\n\n'

    print '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">'
    print '<html><head>'
    
    try:
        form = cgi.FieldStorage()
        
        if form.has_key('login'):
            buildSubmitPage(form)
        else:
            buildFrontPage()
    except LockingError:
        print '<title>Error</title></head><body>'
        print '<h1>Error</h1>'
        print '<p>Could not acquire lock, please try again later.</p>'
    except Exception, e:
        print '<title>Error</title></head><body>'
        print '<h1>Error</h1>'
        print '<p>Internal error!</p>'
        if debug:
            print '<pre>' + str(e) + '</pre>'

    print '</body></html>'

    try:
        unlock()
    except Exception, e:
        if debug:
            print e
