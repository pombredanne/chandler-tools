#   Copyright (c) 2006-2007Open Source Applications Foundation
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

__version__ = "0.1"

__all__ = ['from_feed', 'from_file', 'GData', 'GDataFeed', 'GDataEntry', 'gdata_str', 'gdata_int',
           'gdata_datetime', 'gdata_dict']

from urlparse import urlparse
from dateutil.parser import parse as dateutil_parse
from xml.etree import ElementTree
import httplib, copy, datetime, base64


def find_uri(tree, uri_type):
    """Find the first uri or uri_type in tree's children"""
    for element in tree.getchildren():
        if element.tag.find('link') is not -1:            
            if element.get('type') == 'application/atom+xml' and element.get('rel') == uri_type:
                return element.get('href')
    return None

class make_connection(object):
    """Callable object that returns a new httplib.Connection object"""
    def __init__(self, netloc, connection_class):
        self.netloc = netloc
        self.connection_class = connection_class
    def __call__(self):
        return self.connection_class(self.netloc)
        
def process_urls(urls):
    """Process urls into proper dictionary"""
    for key, url in urls.items():
        if url is not None:
            urls[key] = {}
            urls[key]['url'] = urlparse(url)        
            if urls[key]['url'].scheme == 'http':
                connection_class = httplib.HTTPConnection
            if urls[key]['url'].scheme == 'https':
                connection_class = httplib.HTTPSConnection
            
            urls[key]['make_connection'] = make_connection(urls[key]['url'].netloc, connection_class)
            
    return urls
    
def follow_redirect(url, headers, body):
    url = urlparse(url)
    if url.scheme == 'http':
        connection = httplib.HTTPConnection(url.netloc)
    else:
        connection = httplib.HTTPSConnection(url.netloc)
    connection.request('PUT', url.path, body, headers=headers)
    response = connection.getresponse()
    if response.status == 302:
        follow_redirect(response.getheader('location'), headers, body)
    return response
    
def make_google_auth_headers(email, password, headers, service):
    connection = httplib.HTTPSConnection('www.google.com')
    
    headers = copy.copy(headers)
    headers.update({'content-type':'application/x-www-form-urlencoded'})
    body = 'accountType=HOSTED_OR_GOOGLE&Email=%s&Passwd=%s&service=%s&source=osaf-libgdata-0.1' % (email, password, service)
    connection.request('POST', '/accounts/ClientLogin', body=body, headers=headers)
    response = connection.getresponse()
    
    assert response.status == 200
    response.body = response.read()
    authentication = {}
    for line in response.body.splitlines():
        key, value = line.split('=')
        authentication[key] = value
    assert authentication.has_key('Auth')
    headers['authorization'] = 'GoogleLogin auth=%s' % authentication['Auth']
    return headers
    

class Connect(object):
    """_connect class for maintaining and creating http connections with niceties for gdata"""
    
    headers = {'user-agent':'osaf-libgdata-0.1'}
    
    def __init__(self, tree):
        """Construct a full featured connection object"""
        urls = {'self':find_uri(tree, 'self'),'edit':find_uri(tree, 'edit')}
        self.urls = process_urls(urls)        
        self.tree = tree
    
    def authenticate(self, user, password, service='xapi'):
        """Do google authentication and set proper headers for subsequent requests"""
        return make_google_auth_headers(user, password, self.headers, service)
        
    def request_save(self, user, password, service='xapi'):
        """Make a request to save new data"""
        self.authenticate(user, password, service=service)
        if self.urls['edit'] is None:
            raise Exception, 'Element does not support editing'
        connection = self.urls['edit']['make_connection']()

        headers = copy.copy(self.headers)
        headers.update({'content-type':'application/atom+xml'})
        connection.request('PUT', self.urls['edit']['url'].path, ElementTree.tostring(self.tree), headers=headers)
            
        response = connection.getresponse()
        if response.status == 302:
            response = follow_redirect(response.getheader('location'), headers, ElementTree.tostring(self.tree))
        assert response.status == 200
        response.body = response.read()
        self.last_response = response
        return response.body
    
    def request_refresh(self):
        """Make request to 'self' url and refresh the tree."""
        connection = self.urls['self']['make_connection']()
        
        connection.request('GET', self.urls['self']['url'].path, body=None, headers=self.headers)
        response = connection.getresponse()
        if response.status == 302:
            response = follow_redirect(response.getheader('location'), headers, ElementTree.tostring(self.tree))
        assert response.status == 200
        response.body = response.read()
        self.last_response = response
        return response.body
            
        
class _mock(object):
    pass
        
class gdata_str(unicode):
    """str object that supports __setattr__ and parses string from element"""
    pass
                
class gdata_int(int):
    """int object that supports __setattr__"""
    pass
    
class gdata_datetime(datetime.datetime):
    pass
        
class gdata_dict(dict):
    """dict object replacement to handle changes to element aswell"""
    def __init__(self, items, element, namespace=''):
        dict.__init__(self, items)
        self._element = element
        if namespace != '':
            self.namespace = '{'+namespace+'}'
        else:
            self.namespace = ''
    
    def __setitem__(self, key, value):
        """Custom setitem method to handle setting etree objects upon dict assignment"""
        item = self._element.get(key)
        if item is not None:
            # If the element has key then it's not a dict of gdata entry objects that we've generated
            self._element.set(key, value)
            dict.__setitem__(self, key, value)
        else:
            # This iteration can probably be cleaned up later with a set lookup
            for elem in self._element:
                if elem.tag == self.namespace+key:
                    # Handle each type properly. ## This is getting bigger and bigger, may want to do a dict soon
                    if type(value) is str:
                        assert type(self[key]) is gdata_str
                        elem.text = unicode(value, 'utf-8')
                        dict.__setitem__(self, key, _text(elem))
                    elif type(value) is unicode:
                        assert type(self[key]) is gdata_str
                        elem.text = value
                        dict.__setitem__(self, key, _text(elem))
                    elif type(value) is int:
                        assert type(self[key]) is gdata_int
                        elem.text = unicode(str(value), 'utf-8')
                        dict.__setitem__(self, key, _int_text(elem))
                    elif type(value) is datetime.datetime:
                        assert type(self[key]) is gdata_datetime
                        elem.text = unicode(value.isoformat(), 'utf-8')
                        dict.__setitem__(self, key, _dtime(elem))
                        
        
class gdata_dict_processor(object):
    """Process an element list into an gdata_dict object"""
    def __init__(self, definitions, namespace=''):
        self.namespace = namespace
        self.definitions = definitions
    def __call__(self, element):
        elem_dict = {}
        for elem in element:
            # handle namespaces
            if self.namespace != '':
                key = elem.tag.split('}')
                key = key[1]
            else:
                key = elem.tag
            elem_dict[key] = self.definitions[key](elem)
        for key, value in element.items():
            mock = _mock()
            mock.text = value
            elem_dict[key] = self.definitions[key](mock)
        return gdata_dict(elem_dict.items(), element, namespace=self.namespace)
        
        
# Element parsing
def _dict(element):
    return gdata_dict(element.items(), element)
        
def _dtime(element):
    dt = dateutil_parse(element.text)
    elem_dt = gdata_datetime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, dt.microsecond, dt.tzinfo)
    elem_dt._element = element
    return elem_dt
    
def _text(element):
    elem = gdata_str(element.text)
    elem._element = element
    return elem

def _int_text(element):
    elem_int = gdata_int(int(element.text))
    elem_int._element = element 
    return elem_int
    

class _feed_entry(object):
    """Feed entry callable."""
    def __init__(self, entry_cls, parent=None):
        self.entry_cls = entry_cls
        self.parent = parent
    
    def __call__(self, element):
        return self.entry_cls(element, self.parent)
    

_default_actions = {'id':_text, 'updated':_dtime, 'title':_text, 'link':_dict, 'published':_dtime,
                  'author':gdata_dict_processor({'name':_text, 'email':_text}), 'generator':_dict,    
                  'content':_text, 'category':_dict, 'summary':_text, 'subtitle':_text}
                 
_atom_actions = copy.copy(_default_actions)
                 
_atom_actions['author'] = gdata_dict_processor({'name':_text, 'email':_text}, 'http://www.w3.org/2005/Atom')
                 
_opensearch_actions = {'totalResults':_int_text, 'startIndex':_int_text, 'itemsPerPage':_int_text}

        
            
class GData(object):
    """GData base class"""
    
    _attribute_exclutions = set(['items', 'save'])
    _cleanup_attributes = {'link':'links', 'entry':'entries'}
    
    def __init__(self, tree, parent=None):
        """GData object initializaton. \n
           Handles assignment of etree object to self and creation of _connect object if one is not given. \n
           Handles parsing from etree object to proper tree of gdata objects. Also handles name cleanup."""
        self._tree = tree
        self._parent = parent
        self._connect = Connect(self._tree)
        self._parse_etree(self._tree)
        self._cleanup()
        
    def _process_attribute(self, tag, element, actions):
        if hasattr(self, tag) and type(getattr(self, tag)) is not list:
            value = getattr(self, tag)
            setattr(self, tag, [value, actions[tag](element)])
        elif hasattr(self, tag):
            getattr(self, tag).append(actions[tag](element))
        else:
            print tag, element
            setattr(self, tag, actions[tag](element))
        
    def _parse_etree(self, element_tree):
        for element in element_tree:
            if element.tag.find('{') is not -1:
                namespace, sep, tag = element.tag.partition('}')
                namespace = namespace.replace('{', '')
            else:
                namespace = 'default'
                tag = element.tag
            self._process_attribute(tag, element, self._actions[namespace])
            
    def _remove_attributes(self):
        # Remove all the attributes added 
        for namespace, actions in self._actions.items():
            for key in actions.keys():
                if hasattr(self, key):
                    delattr(self, key)
        for attribute in self._cleanup_attributes.values():
            if hasattr(self, attribute):
                delattr(self, attribute)
            
    # Make object work with dictionary syntax
    def __getitem__(self, key):
        if key not in self._attribute_exclutions:
            return self.__getattr__(self, key)
    def __setitem__(self, key, value):
        if key not in self._attribute_exclutions:
            return self.__setattr__(self, key, value)
    def __delitem__(self, key):
        if key not in self._attribute_exclutions:
            return delattr(self, key)
    
    # Override setattr to make sure _element is set
    def __setattr__(self, key, value):
        # We shouldn't assume every object assigned to a gdata object is actually going to be in the tree
        if hasattr(self, key):
            attribute = getattr(self, key)
            # Handle all the types properly
            if type(value) is str: 
                assert type(getattr(self, key)) is gdata_str
                attribute._element.text = unicode(value, 'utf-8')
                object.__setattr__(self, key, _text(attribute._element))
            elif type(value) is unicode:
                assert type(getattr(self, key)) is gdata_str
                attribute._element.text = value
                object.__setattr__(self, key, _text(attribute._element))              
            elif type(value) is int:
                assert type(getattr(self, key)) is gdata_int
                attribute._element.text = unicode(str(value), 'utf-8')
                object.__setattr__(self, key, _int_text(attribute._element))
            elif type(value) is datetime.datetime:
                assert type(getattr(self, key)) is gdata_datetime
                attribute._element.text = unicode(value.isoformat(), 'utf-8')
                object.__setattr__(self, key, _dtime(attribute._element))
            else:
                object.__setattr__(self, key, value)
        else:
            object.__setattr__(self, key, value)

    def items(self):
        item_list = []
        for attribute in dir(self):
            if not attribute.startswith('_') and attribute not in self._attribute_exclutions:
                item_list.append((attribute, getattr(self, attribute)))
        return item_list
                        
    def _cleanup(self):
        # Make elements of the same name prettier -- this can be overriden for further cleanup
        for key, value in self._cleanup_attributes.items():
            if hasattr(self, key) and type(getattr(self, key)) is list:
                setattr(self, value, getattr(self, key))
                delattr(self, key)
                
    def refresh(self):
        if self._parent is None:
            tree = ElementTree.fromstring(self._connect.request_refresh())
            self._remove_attributes()
            self.__init__(tree)
        else:
            self._parent.refresh()


class GDataEntry(GData):
    _actions = {}
    _actions['http://www.w3.org/2005/Atom'] = _atom_actions
    _actions['default'] = _default_actions
    
    def save(self, user, password, service=None):
        """Save for entry objects"""
        if service is None:
            if hasattr(self, '_service'):
                service = self._service
            else:
                service = 'xapi'
        body = self._connect.request_save(user, password, service)
        tree = ElementTree.fromstring(body)
        self.refresh()
        
class GDataFeed(GData):
    _actions = {}
    _actions['default'] = _default_actions
    _actions['http://a9.com/-/spec/opensearchrss/1.0/'] = _opensearch_actions

    def __init__(self, *args):
        self._actions['http://www.w3.org/2005/Atom'] = copy.copy(_atom_actions)
        self._actions['http://www.w3.org/2005/Atom']['entry'] = _feed_entry(GDataEntry, parent=self)
        GData.__init__(self, *args)
    
class BloggerEntry(GDataEntry):
    _service = 'blogger'
    
class BloggerFeed(GDataFeed):
    _service = 'blogger'

    def __init__(self, *args):
        self._actions['http://www.w3.org/2005/Atom'] = copy.copy(_atom_actions)
        self._actions['http://www.w3.org/2005/Atom']['entry'] = _feed_entry(BloggerEntry, parent=self)
        GData.__init__(self, *args)
        
class CalendarEntry(GDataEntry):
    _service = 'cl'
    
    _actions = {}
    _actions['default'] = _default_actions
    _actions['http://a9.com/-/spec/opensearchrss/1.0/'] = _opensearch_actions
    _actions['http://www.w3.org/2005/Atom'] = _atom_actions
    _actions['http://schemas.google.com/gCal/2005'] = {'timezone':_dict, 'sendEventNotifications':_dict}
    _actions['http://schemas.google.com/g/2005'] = {'where':_dict, 'transparency':_dict, 
                                                    'eventStatus':_dict,
                                                    'comments':gdata_dict_processor({'feedLink':_dict}, namespace='http://schemas.google.com/g/2005'), 
                                                    'visibility':_dict,
                                                    'when':gdata_dict_processor({'endTime':_dtime, 'startTime':_dtime, 'reminder':_dict}, namespace='http://schemas.google.com/g/2005'),
                                                    'recurrence':_text, 'who':_dict, 'reminder':_dict}
    _cleanup_attributes = {'link':'links', 'entry':'entries', 'who':'attendees'}
    
class CalendarFeed(GDataEntry):
    _service = 'cl'
    
    _actions = {}
    _actions['default'] = _default_actions
    _actions['http://a9.com/-/spec/opensearchrss/1.0/'] = _opensearch_actions
    _actions['http://schemas.google.com/gCal/2005'] = {'timezone':_dict, 'sendEventNotifications':_dict}
    _new = {}
    _new['link'] = 'http://schemas.google.com/g/2005#post'
    
    def __init__(self, *args):
        self._actions['http://www.w3.org/2005/Atom'] = copy.copy(_atom_actions)
        self._actions['http://www.w3.org/2005/Atom']['entry'] = _feed_entry(CalendarEntry, parent=self)
        GData.__init__(self, *args)
    
        
# Gmail doesn't have a full gdata api yet        
        
# class GmailEntry(GDataEntry):
#     _service = 'mail'
#     
# class GmailFeed(GDataFeed):
#     _service = 'mail'
#     def __init__(self, *args):
#         self._actions['http://www.w3.org/2005/Atom'] = copy.copy(_atom_actions)
#         self._actions['http://www.w3.org/2005/Atom']['entry'] = _feed_entry(GmailEntry, parent=self)
#         GData.__init__(self, *args)
                        
_auto_create = {'blogger.com':BloggerFeed, }

def get_calendar(email, password):
    headers = make_google_auth_headers(email, password, Connect.headers, 'cl')
    headers['content-type'] = 'application/atom+xml'
    return from_feed('http://www.google.com/calendar/feeds/'+email+'/private/full', 
                      headers=headers, feed_class=CalendarFeed)

def from_feed(uri, headers={}, feed_class=GDataFeed):
    """Create gata object from feed uri"""
    url = urlparse(uri)
    if url.scheme == 'http':
        connection = httplib.HTTPConnection(url.netloc)
    else:
        connection = httplib.HTTPSConnection(url.netloc)

    connection.request('GET', url.path, headers=headers)
    response = connection.getresponse()
    response.body = response.read()
    print response.body
    tree = ElementTree.fromstring(response.body)
    # return tree
    gdata = feed_class(tree)
    return gdata

def from_file(filename):
    """Create gdata object from file"""
    f = open(filename, 'r')
    tree = ElementTree.fromstring(f.read())
    gdata = GDataFeed(tree)
    return gdata
    